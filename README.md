# Gleam - a language grammar for highlight.js

![version](https://badgen.net/npm/v/highlightjs-gleam) ![license](https://badgen.net/badge/license/MIT/blue)
![install size](https://badgen.net/packagephobia/install/highlightjs-gleam) ![minified size](https://badgen.net/bundlephobia/min/highlightjs-gleam)

## Usage

Simply include the Highlight.js library in your webpage or Node app, then load this module.

### Static website or simple usage

Simply load the module after loading Highlight.js. You'll use the minified version found in the `dist` directory. This module is just a CDN build of the language, so it will register itself as the Javascript is loaded.

```html
<script type="text/javascript" src="/path/to/highlight.min.js"></script>
<script
  type="text/javascript"
  charset="UTF-8"
  src="/path/to/highlightjs-gleam/dist/gleam.min.js"
></script>
<script type="text/javascript">
  hljs.initHighlightingOnLoad();
</script>
```

### Using directly from the UNPKG CDN

```html
<script
  type="text/javascript"
  src="https://unpkg.com/highlightjs-gleam@0.2.2/dist/gleam.min.js"
></script>
```

- More info: <https://unpkg.com>

### With Node or another build system

If you're using Node / Webpack / Rollup / Browserify, etc, simply require the language module, then register it with Highlight.js.

```javascript
var hljs = require("highlightjs");
var hljsGleam = require("highlightjs-gleam");

hljs.registerLanguage("gleam", hljsGleam);
hljs.initHighlightingOnLoad();
```

## Links

- The official site for the Highlight.js library is <https://highlightjs.org/>.
- The Highlight.js GitHub project: <https://github.com/highlightjs/highlight.js>
- Learn more about Gleam: <https://gleam.run>

## Contributing

Contributions are welcome!

When creating a PR be sure to describe what changes you are making and provide
screenshots of before and after.

Be sure to run `yarn minify` and describe your change in `CHANGELOG.md`.
